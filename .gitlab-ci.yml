###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
stages:
  - test
  - run
  - build
  - deploy

image: registry.cern.ch/docker.io/library/python:3.8

.setup_environment:
  before_script:
    - pip install .[testing]

build_kaniko_command:
    stage: build
    variables:
      IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}:latest
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    script:
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        - /kaniko/executor --context $CI_PROJECT_DIR/doxy_image --dockerfile $CI_PROJECT_DIR/doxy_image/Dockerfile --destination $IMAGE_DESTINATION
        - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
    only:
      changes:
        - doxy_image/Dockerfile
        - doxy_image/eos9-x86_64.repo

pre-commit:
  stage: test
  rules:
    - when: always
  before_script:
    - pip install pre-commit
    # TODO: There should be a proper way of doing this
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - chmod +x lb-check-copyright
  script:
    - if [ "$CI_COMMIT_BRANCH" == "master" ] ; then
    -   pre-commit run --show-diff-on-failure --all-files
    - else
    -   git fetch origin master
    -   pre-commit run --show-diff-on-failure --from-ref FETCH_HEAD --to-ref HEAD
    - fi
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}

pylint:
  extends: .setup_environment
  stage: test
  rules:
    - when: always
  script:
    - pylint src/lhcbstacks

pytest:
  extends: .setup_environment
  stage: test
  rules:
    - when: always
  script:
    - pytest

generate_stacks:
  extends: .setup_environment
  stage: run
  tags:
    - cvmfs
  rules:
    - if: '$CI_COMMIT_BRANCH != "master"'
  artifacts:
    paths:
      - to_release.json
    expire_in: 1 week
  script:
    - git fetch origin master
    - lb-stacks-to-release /cvmfs/lhcb.cern.ch/lib/ ./data/stacks $(git diff --name-only --diff-filter=ACRM FETCH_HEAD HEAD | grep "^data/stacks/.*\.yml") | tee to_release.json

generate_project_list:
  extends: .setup_environment
  stage: run
  rules:
    - when: always
  artifacts:
    paths:
      - projects.json
    expire_in: 1 year
  script:
    - lb-projects ./data/projects.yml | tee projects.json

generate_projects_platforms:
  extends: .setup_environment
  stage: run
  tags:
    - cvmfs
  rules:
    - when: always
  artifacts:
    paths:
      - projects_platforms.json
    expire_in: 1 year
  script:
    - lb-projects-platforms /cvmfs/lhcb.cern.ch/lib/ ./data/stacks | tee projects_platforms.json

generate_projects_info:
  extends: .setup_environment
  stage: run
  tags:
    - cvmfs
  rules:
    - when: always
  artifacts:
    paths:
      - projects_info.json
    expire_in: 1 year
  script:
    - lb-projects-info /cvmfs/lhcb.cern.ch/lib/ ./data/stacks | tee projects_info.json

generate_lcg_info:
  extends: .setup_environment
  stage: run
  tags:
    - cvmfs
  rules:
    - when: always
  artifacts:
    paths:
      - lcg_info.json
    expire_in: 1 year
  script:
    - lb-lcg-info /cvmfs/lhcb.cern.ch/lib/ ./data/stacks ./data/lcg/lcg_info_legacy.json -o lcg_info.json

trigger_deploy_to_cvmfs:
  extends: .setup_environment
  stage: deploy
  only:
    - master
  script:
    - echo "Running lb-deploy-metadata ${CI_PIPELINE_ID}"
    - lb-deploy-metadata "${CI_PIPELINE_ID}"

deploy-job:
  image: gitlab-registry.cern.ch/lhcb-core/lhcbstacks:latest
  tags:
    - cvmfs
  stage: deploy
  environment: production
  script:
    - source /cvmfs/lhcb.cern.ch/lib/LbEnv
    - pip install LbConfiguration
    - pip install LbReleaseDoxy
    - export LCG_release_area=/cvmfs/sft.cern.ch/lcg/releases
    - bash doxy_documentation/ci_run_doxy_multiple_commits.sh
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - data/stacks/**/*
  artifacts:
    paths:
      - job.log
    expire_in: 2 weeks
