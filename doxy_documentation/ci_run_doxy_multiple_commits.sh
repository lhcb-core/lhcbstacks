#!/bin/bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# A script to be executed by the ci in https://gitlab.cern.ch/lhcb-core/lhcbstacks/ via a Docker image that creates Doxygen documentation when new stack files are committed
klist
kinit $EOS_USER <<< $EOS_PASSWORD
klist
INFILENAME=$(git diff HEAD~ HEAD --name-only | grep -E '^data/stacks/.*\.yml$')
touch job.log
echo $INFILENAME | tee -a job.log
for file in $INFILENAME
do
    echo "Processing $file" | tee -a job.log
    part1=$(dirname "$file")
    PROJECTNAME=$(basename $part1)
    FILENAME=$(basename "$file")
    PROJECTVERSION=${FILENAME//.yml/}
    DIRNAME=$PROJECTNAME"_"$PROJECTVERSION
    mkdir $DIRNAME
    echo "------------------------------------------------"
    cd $DIRNAME
    grep -v 'name: ' $CI_PROJECT_DIR/$file |
    grep ': v' |
    while read line; do
        VERSION=`echo ${line} | awk '{print $2}'`
        PROJECT=`echo ${line} | awk '{print substr($1, 1, length($1)-1)}'`
        GIT="git clone -b "
        GIT2=".git"
        ADDRESS=" https://gitlab.cern.ch/lhcb/"
        CLONENAME=" ${PROJECT^^}""_""$VERSION"


        CLONE="$GIT$VERSION$ADDRESS$PROJECT$GIT2$CLONENAME"
        echo $CLONE
        $CLONE
    done

    #zip -r "${DIRNAME:5}.zip" *
    LCG_VERSION=`grep 'version: ' $CI_PROJECT_DIR/$file | awk '{print $2}'`
    cd ../
    python3 doxy_documentation/LHCbDoc.py --input $CI_PROJECT_DIR/$DIRNAME --debug --lcg_version $LCG_VERSION | tee -a job.log
    mkdir -p $CI_PROJECT_DIR/$PROJECTNAME/$PROJECTVERSION
    mv  $CI_PROJECT_DIR/$DIRNAME/$DIRNAME.zip $PROJECTNAME/$PROJECTVERSION
    mv $CI_PROJECT_DIR/$DIRNAME/search/ $PROJECTNAME/$PROJECTVERSION
    mv $CI_PROJECT_DIR/$DIRNAME/search* $PROJECTNAME/$PROJECTVERSION
    cp $CI_PROJECT_DIR/doxy_documentation/extract.php $PROJECTNAME/$PROJECTVERSION
    echo "Writing .htaccess file" | tee -a job.log
    python3 doxy_documentation/write_htaccess.py --version $PROJECTVERSION --project $PROJECTNAME
    cp $CI_PROJECT_DIR/doxy_documentation/.htaccess $PROJECTNAME/$PROJECTVERSION
    echo "Copying db.json from eos" | tee -a job.log
    export EOS_MGM_URL=root://eosproject-l.cern.ch/
    eos cp /eos/project/l/lhcbwebsites/www/doxygen/docs/db.json $CI_PROJECT_DIR/doxy_documentation/redirect/ | tee -a job.log
    echo "Running database.py" | tee -a job.log
    python3 doxy_documentation/redirect/database.py --version $PROJECTVERSION --project $PROJECTNAME --yaml $file | tee -a job.log
    cp $CI_PROJECT_DIR/$file $PROJECTNAME/$PROJECTVERSION
    echo "copying back to eos" | tee -a job.log
    eos cp -r  $CI_PROJECT_DIR/$PROJECTNAME /eos/project/l/lhcbwebsites/www/doxygen/stacks/ | tee -a job.log
    eos cp $CI_PROJECT_DIR/doxy_documentation/redirect/.htaccess /eos/project/l/lhcbwebsites/www/doxygen/docs/ | tee -a job.log
    eos cp $CI_PROJECT_DIR/doxy_documentation/redirect/db.json /eos/project/l/lhcbwebsites/www/doxygen/docs/ | tee -a job.log
    rm doxy_documentation/redirect/.htaccess
    rm doxy_documentation/redirect/db.json
done
