#!/bin/bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# this should run on lxplus, or something with LbEnv sourced as long as the copy to eos command is commented out
INFILENAME=$1 #This needs to be changed eventually as right now we need a path to the yml file
echo $INFILENAME
part1=$(dirname "$INFILENAME")
PROJECTNAME=$(basename $part1)
FILENAME=$(basename "$INFILENAME")
PROJECTVERSION=${FILENAME//.yml/}

INPUTNAME=$PROJECTNAME"_"$PROJECTVERSION
DIRNAME=$INPUTNAME
echo $DIRNAME
#mkdir $ROOTNAME

mkdir $DIRNAME
echo "------------------------------------------------"
cd $DIRNAME
grep -v 'name: ' ../$INFILENAME |
grep ': v' |
while read line; do
	VERSION=`echo ${line} | awk '{print $2}'`
	PROJECT=`echo ${line} | awk '{print substr($1, 1, length($1)-1)}'`
	GIT="git clone -b "
    GIT2=".git"
	ADDRESS=" https://gitlab.cern.ch/lhcb/"
  CLONENAME=" ${PROJECT^^}""_""$VERSION"


	CLONE="$GIT$VERSION$ADDRESS$PROJECT$GIT2$CLONENAME"
    echo $CLONE
	$CLONE
done
cd ../
LCG_VERSION=`grep 'version: ' $INFILENAME | awk '{print $2}'`
python3 LHCbDoc.py --input $PWD/$DIRNAME --debug --lcg_version $LCG_VERSION | tee job.log
mkdir $PROJECTNAME
mkdir $PROJECTNAME/$PROJECTVERSION
mv  $DIRNAME/$DIRNAME.zip $PROJECTNAME/$PROJECTVERSION
cp extract.php $PROJECTNAME/$PROJECTVERSION
python3 write_htaccess.py
cp .htaccess $PROJECTNAME/$PROJECTVERSION
cp $INFILENAME $PROJECTNAME/$PROJECTVERSION
#cp $PROJECTNAME pinogga@lxplus.cern.ch:/eos/project/l/lhcbwebsites/www/doxygen/stacks
echo "Removing source directory from local path"
rm -rf $PWD/$DIRNAME
#rm -rf $PROJECTNAME
