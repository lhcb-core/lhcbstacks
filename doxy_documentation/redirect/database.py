###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import re

import yaml

DOCS_HTACCESS_HEAD = """# GENERATED FILE: DO NOT EDIT
Options -Indexes
RewriteEngine On
# this is a clever trick for avoiding RewriteBase
# see http://stackoverflow.com/a/21063276
RewriteCond "%{REQUEST_URI}::$1" "^(.*?/)(.*)::\\2$"
RewriteRule "^(.*)$" "-" [E=BASE:%1]
# add /index.html to the top entry
RewriteRule "^([^/]+/[^/]+)/?$" "%{ENV:BASE}$1/index.html" [R,L]
# redirect to the content of the right .zip file
"""
DOCS_HTACCESS_LINE = 'RewriteRule "^{pv}/(.+)" ' '"%{{ENV:BASE}}../{doc}/$1" [PT,L]\n'
DOCS_HTACCESS_LATEST_LINE = (
    'RewriteRule "^{p}/latest/(.+)" ' '"%{{ENV:BASE}}{path}/$1" [PT,L]\n'
)


class DocsDB(object):
    def __init__(self, yml):
        self.yml = yml
        with open("doxy_documentation/redirect/db.json") as json_file:
            self.data = json.load(json_file)
        with open(self.yml, "r") as yml_file:
            self.yml_data = yaml.safe_load(yml_file)

    def write_json(self, project, version):
        for keys_yml in self.yml_data["projects"]:
            string = keys_yml.lower() + "/" + self.yml_data["projects"][keys_yml]
            self.data[string] = "stacks/{0}/{1}".format(project, version)
        self.data_keys = list(self.data.keys())
        self.data_keys.sort()
        self.data = dict(
            sorted(
                self.data.items(),
                key=lambda item: (
                    re.search(r"([^/]+)/", item[0]).group(1)
                    if re.search(r"([^/]+)/", item[0])
                    else "",
                    int(re.search(r"(\d+)", item[0]).group())
                    if re.search(r"(\d+)", item[0])
                    else 0,
                ),
            )
        )
        with open("doxy_documentation/redirect/db.json", "w") as f:
            json.dump(self.data, f, indent=2)

    def latest_version(self, project):
        project_keys = [key for key in self.data.keys() if project.lower() in key]
        return project_keys[-1]

    def write_htaccess(self, project):
        result = []
        for key in self.data.keys():
            substring = key.split("/")[0]
            if substring not in result:
                result.append(substring)
        with open("doxy_documentation/redirect/.htaccess", "w") as f:
            f.write(DOCS_HTACCESS_HEAD)
            f.writelines(
                DOCS_HTACCESS_LINE.format(pv=pv, doc=self.data[pv])
                for pv in self.data.keys()
            )
            f.write('# redirect special version "latest"\n')
            f.writelines(
                DOCS_HTACCESS_LATEST_LINE.format(p=p, path=self.latest_version(p))
                for p in result
            )


def main():
    import argparse

    parser = argparse.ArgumentParser(
        prog="database",
        description="Write the db.json and .htaccess file for doxygen deployment",
    )
    parser.add_argument("-p", "--project", action="store", help="Project name")
    parser.add_argument("-v", "--version", action="store", help="Project version")
    parser.add_argument("-y", "--yaml", action="store", help="yaml file")
    args = parser.parse_args()

    docs = DocsDB(args.yaml)
    docs.write_json(args.project, args.version)
    docs.write_htaccess(args.project)


if __name__ == "__main__":
    main()


# for key in data:
#    print(key, data[key])
