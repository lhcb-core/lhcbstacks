###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def write(project, version):
    with open("doxy_documentation/.htaccess", "w") as f:
        f.write(
            "#Taken from https://gitlab.cern.ch/lhcb-core/LbScripts/-/blob/master/LbRelease/data/eos_doxygen_support_files/.htaccess\n"
        )
        f.write("Options -Indexes\n")
        f.write("DirectoryIndex index.php index.html index.htm \n")
        f.write("RewriteEngine On\n")
        f.write('RewriteCond "%{REQUEST_URI}::$1" "^(/[^/]+(/.*?/))(.*)::\\3$"\n')
        f.write('RewriteRule "^(.*)$" "-" [E=BASE:%1,E=FILENAME:%2%3]\n')
        f.write('RewriteCond "%{DOCUMENT_ROOT}%{ENV:FILENAME}" !-f\n')
        f.write(
            'RewriteRule "^(..+)" "%{{ENV:BASE}}extract.php?zip={0}_{1}.zip&path={0}_{1}/$1" [PT,L]\n'.format(
                project, version
            )
        )
        f.write('RewriteRule "^/?$" "%{ENV:BASE}index.html" [R,L]\n')
        f.close()


def main():
    import argparse

    parser = argparse.ArgumentParser(
        prog="write_htaccess",
        description="Write the .htaccess file for doxygen deployment",
    )
    parser.add_argument("-p", "--project", action="store", help="Project name")
    parser.add_argument("-v", "--version", action="store", help="Project version")
    args = parser.parse_args()

    write(args.project, args.version)


if __name__ == "__main__":
    main()
