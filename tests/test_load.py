###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from pathlib import Path

import pytest

import lhcbstacks

STACK_DIR = Path(__file__).parent / "data" / "stacks"
PROJECTS_DATA = Path(__file__).parent / "data" / "projects.yml"
LHCBSITEROOT = "/cvmfs/lhcb.cern.ch/lib"
TEST_STACK = "Run3Analysis202008"
TEST_PROJECT = "DaVinci"
TEST_VERSION = "v52r0"
PLATFORMS = [
    "x86_64+avx2+fma-centos7-gcc9-opt",
    "x86_64-centos7-clang8-dbg",
    "x86_64-centos7-clang8-opt",
    "x86_64-centos7-gcc9-dbg",
    "x86_64-centos7-gcc9-do0",
    "x86_64-centos7-gcc9-opt",
]
TEST_PLATFORM = "x86_64-centos7-gcc9-opt"


@pytest.fixture
def lhcb_install_area(tmp_path):
    """Create a fake install with a missing platform and missing projects"""
    lhcb_dir = tmp_path
    s = lhcbstacks.Stack.load(STACK_DIR, TEST_STACK, lhcb_dir)

    binary_paths = s.binary_paths()
    binary_paths.pop("x86_64+avx2+fma-centos7-gcc9-opt")
    binary_paths.pop("x86_64-centos7-gcc9-do0")
    tmp = binary_paths["x86_64-centos7-clang8-dbg"][:]
    binary_paths["x86_64-centos7-clang8-dbg"] = [
        p for p in tmp if "DAVINCI" not in str(p)
    ]
    for _, v in binary_paths.items():
        for p, _, _ in v:
            os.makedirs(p)
    return lhcb_dir


def test_load_info():
    stack_info = lhcbstacks._load_stack_file(
        lhcbstacks.stack_filename(STACK_DIR, TEST_STACK)
    )
    assert stack_info["type"] == "lhcbcmake"


def test_load_stack():
    a = lhcbstacks.Stack.load(STACK_DIR, TEST_STACK, LHCBSITEROOT)
    ps = [p for (p, _, _) in a.project_paths()]
    dv = [p for p in ps if TEST_PROJECT.upper() in str(p)]
    assert (
        str(dv[0])
        == f"{LHCBSITEROOT}/lhcb/{TEST_PROJECT.upper()}/{TEST_PROJECT.upper()}_{TEST_VERSION}"
    )


def test_list_platforms():
    a = lhcbstacks.Stack.load(STACK_DIR, TEST_STACK, LHCBSITEROOT)
    assert a.platforms() == PLATFORMS


def test_list_stacks_in_dir():
    stack_list = list(lhcbstacks._list_stacks_in_dir(STACK_DIR))
    assert len(stack_list) == 3


def test_list_binary_paths():
    a = lhcbstacks.Stack.load(STACK_DIR, TEST_STACK, LHCBSITEROOT)
    dv = [p for p in a.binary_paths()[TEST_PLATFORM] if TEST_PROJECT.upper() in str(p)]
    assert (
        str(dv[0][0])
        == f"{LHCBSITEROOT}/lhcb/{TEST_PROJECT.upper()}/{TEST_PROJECT.upper()}_{TEST_VERSION}/InstallArea/{TEST_PLATFORM}"
    )


def test_missing(lhcb_install_area):
    a = lhcbstacks.Stack.load(STACK_DIR, TEST_STACK, lhcb_install_area)
    missing = a.missing_projects()

    # We should be missing x86_64+avx2+fma-centos7-gcc9-opt, and DV for x86_64-centos7-clang8-dbg
    assert len(missing["x86_64+avx2+fma-centos7-gcc9-opt"]) == 8
    assert len(missing["x86_64-centos7-clang8-dbg"]) == 1
    assert len(missing) == 3


def test_incomplete_stacks(lhcb_install_area):
    inc_stacks = lhcbstacks.incomplete_stacks(STACK_DIR, lhcb_install_area)
    assert len(inc_stacks) == 3


def test_stacks_to_release(lhcb_install_area):
    release = lhcbstacks.list_stacks_to_release(STACK_DIR, lhcb_install_area)
    result_json = json.dumps(release, sort_keys=True)
    expected = '[{"build_tool": "cmt", "deployed": {"LCG": "60"}, "name": "CMTStack", "platforms": ["x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"], "projects": {"Gaudi": "v1r1", "LCG": "60", "LHCb": "v1r1"}}, {"build_tool": "cmake", "deployed": {"LCG": "97a"}, "name": "Gaudi_v35r2", "platforms": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"], "projects": {"Gaudi": "v35r2", "LCG": "97a"}}, {"build_tool": "cmake", "deployed": {"LCG": "97a"}, "name": "Run3Analysis202008", "platforms": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-gcc9-do0"], "projects": {"Analysis": "v32r0", "DaVinci": "v52r0", "Detector": "v0r5", "Gaudi": "v33r2", "LCG": "97a", "LHCb": "v51r1", "Lbcom": "v31r1", "Phys": "v31r1", "Rec": "v31r1"}}, {"build_tool": "cmake", "deployed": {"Analysis": "v32r0", "Detector": "v0r5", "Gaudi": "v33r2", "LCG": "97a", "LHCb": "v51r1", "Lbcom": "v31r1", "Phys": "v31r1", "Rec": "v31r1"}, "name": "Run3Analysis202008-1", "platforms": ["x86_64-centos7-clang8-dbg"], "projects": {"Analysis": "v32r0", "DaVinci": "v52r0", "Detector": "v0r5", "Gaudi": "v33r2", "LCG": "97a", "LHCb": "v51r1", "Lbcom": "v31r1", "Phys": "v31r1", "Rec": "v31r1"}}]'
    assert result_json == expected


def test_list_projects():
    release = lhcbstacks.list_projects(PROJECTS_DATA)
    result_json = json.dumps(release, sort_keys=True)
    expected = '[{"name": "LHCb", "sourceuri": "gitlab-cern:lhcb/LHCb"}, {"name": "Gaudi", "sourceuri": "gitlab-cern:Gaudi:Gaudi"}, {"name": "Gauss", "sourceuri": "gitlab-cern:lhcb/Gauss"}]'
    assert result_json == expected


def test_list_projects_platforms(lhcb_install_area):
    result = lhcbstacks.list_projects_platforms(STACK_DIR, lhcb_install_area)
    expected_txt = """
    {"ANALYSIS": {"v32r0": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "DAVINCI": {"v52r0": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "DETECTOR": {"v0r5": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "GAUDI": {"v1r1": ["x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"],
    "v33r2": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"],
    "v35r2": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "LBCOM": {"v31r1": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "LHCB": {"v1r1": ["x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"],
    "v51r1": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "PHYS": {"v31r1": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]},
    "REC": {"v31r1": ["x86_64+avx2+fma-centos7-gcc9-opt", "x86_64-centos7-clang8-dbg", "x86_64-centos7-clang8-opt", "x86_64-centos7-gcc9-dbg", "x86_64-centos7-gcc9-do0", "x86_64-centos7-gcc9-opt"]}}
    """
    expected = json.loads(expected_txt)
    assert set(result["ANALYSIS"]["v32r0"]) == set(expected["ANALYSIS"]["v32r0"])
