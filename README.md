# lhcbstacks

This project contains the configuration for the LHCb software stacks, and the
software to present this metadata in the format required by the build tools.

For some context and introduction to the concept of stacks, please see
[these slides](https://indico.cern.ch/event/1018357/contributions/4279996/attachments/2211976/3743760/New%20release%20deployment%20procedure%20and%20tools.pdf).

## Project structure

  - `data`: directory structure containing the LHCb stack definitions in YAML format
  - `src`, `tests`: Python package to manage the data and associated tests

## Stack metadata file structure

Inside the `data/stacks` directory we have one subdirectory per application
containing one stack definition YAML file per *logical* version of the
application (for example `DaVinci/v53r0.yml` or `Moore/22.06-FEST.yml`).

The stack definition should follow the example below:

```
type: lhcbcmake

projects:
  Analysis: v32r0
  Phys: v31r1
  Rec: v31r1
  Lbcom: v31r1
  LHCb: v51r1
  Gaudi: v33r2
  Detector: v0r5
  DaVinci: v52r1

toolchain:
  type: LCG
  version: 97a

platforms:
  - x86_64+avx2+fma-centos7-gcc9-opt
  - x86_64-centos7-clang8-dbg
  - x86_64-centos7-clang8-opt
  - x86_64-centos7-gcc9-dbg
  - x86_64-centos7-gcc9-do0
  - x86_64-centos7-gcc9-opt

tags:
  - Run3
```

The `type` can be:
  - lhcbcmake
  - cmt

The project list should contain all *LHCb* projects needed in the stack.

The toolchain refers to the release of the external software used by this stack
(e.g. LCG 97a in the example).

The *tags* list is a free list that can be used by software querying this
metadata.

## Validation and development

This package uses *[pre-commit](https://pre-commit.com)* to manage its
pre-commit hooks. Make sure that this Python package is available (for example
from the LHCb environment LbEnv) and make sure to install the hooks in the Git clone with:

```
pre-commit install
```

## Merge Requests and Release Builds

When a *ready* merge request (non-draft) is available, the nightly builds system picks it
up and triggers a build of all projects that are defined in the slot and not already
available on CVMFS.

The *release shifter* on duty will pick up the MR and complete the deployment of the
projects following the check list pre-filled in the merge request description. Once all
tasks are completed, the MR can be merged.

If there is a problem during with the release build, the MR should be marked as *draft*
until the problem is resolved. A new build will be automatically triggered once the
merge request is marked again as *ready* (provided it stayed *draft* for at least 10 minutes).
