###############################################################################
# (c) Copyright 2021-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import logging
from pathlib import Path

import lhcbstacks

from .lcg import find_online_ROOT_version, find_ROOT_version


def create_lcg_legacy_metadata():
    """Command line LHCb project information to a JSON file.

    This requires CVMFS (/cvmfs/lhcb.cern.ch) to find all the installed versions of
    LCG to identify the versions of ROOT. It can use the CMT requirements file or the
    LCG_XXX/ folder on disk containg soft links to the real versions of the software.

    """
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("siteroot")
    parser.add_argument("repository")
    parser.add_argument("-o", "--output", default=None, help="Output file name")
    args = parser.parse_args()
    info = lhcbstacks.list_projects_info(args.repository, args.siteroot)

    # Checking the LCG versions required by the LHCb stacks
    lcgversions = set()
    for project in info:  # pylint: disable=C0206
        for version in info[project]:
            lcgversions.add(info[project][version]["LCG"])

    # This was tested against the following versions
    # lcgversions = {
    #     "104",
    #     "62b",
    #     "69root6",
    #     "58c",
    #     "58e",
    #     "70root6",
    #     "105a",
    #     "76root6",
    #     "64d",
    #     "87",
    #     "60b",
    #     "91",
    #     "86",
    #     "84",
    #     "71root6",
    #     "57a",
    #     "66",
    #     "72root6",
    #     "64a",
    #     "60a",
    #     "68",
    #     "101",
    #     "64c",
    #     "65a",
    #     "58f",
    #     "69",
    #     "64b",
    #     "93",
    #     "102b",
    #     "101a",
    #     "79",
    #     "61b",
    #     "56c",
    #     "96",
    #     "97a",
    #     "61",
    #     "95",
    #     "58a",
    #     "71",
    #     "62a",
    #     "68_root6",
    #     "94",
    #     "88",
    #     "60",
    #     "103",
    #     "58",
    #     "83",
    #     "64",
    #     "58b",
    #     "100",
    #     "65",
    #     "74root6",
    #     "96b",
    # }

    lcginfomap = {}
    missing = []
    for v in lcgversions:
        rootver = find_ROOT_version(args.siteroot, v)
        if rootver:
            lcginfomap[v] = {"ROOT": rootver}  # noqa: B909
        else:
            missing.append(v)
    if missing:
        print(f"Could not identify ROOT version for LCG versions: {', '.join(missing)}")

    if args.output:
        with open(args.output, "w", encoding="UTF-8") as f:
            f.write(json.dumps(lcginfomap, indent=4, sort_keys=True))

    print(json.dumps(lcginfomap, indent=4, sort_keys=True))


def create_lcg_metadata():
    """Command line tool to save LCG information to a file.

    This loads the legacy info from a file and checks for the new versions of LCG online.

    """
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("siteroot")
    parser.add_argument("repository")
    parser.add_argument("lcg_legacy_info", help="Legacy LCG information file")
    parser.add_argument("-o", "--output", default=None, help="Output file name")
    args = parser.parse_args()

    logging.debug("Get LHCb projects information")
    info = lhcbstacks.list_projects_info(args.repository, args.siteroot)

    # Checking the LCG versions required by the LHCb stacks
    lcgversions = set()
    for project in info:  # pylint: disable=C0206
        for version in info[project]:
            lcgversions.add(info[project][version]["LCG"])

    # Loading the file with the legacy versions
    logging.debug("Loading legacy LCG information")
    legacy_file = Path(args.lcg_legacy_info)
    if not legacy_file.exists():
        raise FileNotFoundError(
            f"Legacy LCG information file {legacy_file} does not exist"
        )
    legacy_lcginfomap = json.loads(legacy_file.read_text(encoding="UTF-8"))

    # Checking whether we have new versions of LCG
    missinglcgversions = set(lcgversions) - set(legacy_lcginfomap.keys())
    logging.debug("Missing versions: %s", missinglcgversions)

    # Adding the information for the new versions
    lcginfomap = legacy_lcginfomap
    missing = []
    for v in missinglcgversions:
        rootver = find_online_ROOT_version(v)
        if rootver:
            lcginfomap[v] = {"ROOT": rootver}  # noqa: B909
        else:
            missing.append(v)
    if missing:
        raise RuntimeError(
            f"Could not identify ROOT version for LCG versions: {', '.join(missing)}"
        )

    if args.output:
        with open(args.output, "w", encoding="UTF-8") as f:
            f.write(json.dumps(lcginfomap, indent=4, sort_keys=True))

    print(json.dumps(lcginfomap, indent=4, sort_keys=True))


if __name__ == "__main__":
    create_lcg_metadata()
