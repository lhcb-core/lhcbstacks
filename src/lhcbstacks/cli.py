###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import logging
import os

import lhcbstacks


def stacks_to_release_json():
    parser = argparse.ArgumentParser()
    parser.add_argument("siteroot")
    parser.add_argument("repository")
    parser.add_argument("stack_paths", nargs="*")
    args = parser.parse_args()
    names = None
    if args.stack_paths:
        names = [os.path.relpath(p, start=args.repository) for p in args.stack_paths]
    release = lhcbstacks.list_stacks_to_release(args.repository, args.siteroot, names)
    print(json.dumps(release, indent=4, sort_keys=True))


def projects_json():
    parser = argparse.ArgumentParser()
    parser.add_argument("projects_data")
    args = parser.parse_args()
    projects = lhcbstacks.list_projects(args.projects_data)
    print(json.dumps(projects, indent=4, sort_keys=True))


def projects_platforms_json():
    """Command line to dump the list of projects/platforms  in JSON"""
    parser = argparse.ArgumentParser()
    parser.add_argument("siteroot")
    parser.add_argument("repository")
    args = parser.parse_args()
    projects_platforms = lhcbstacks.list_projects_platforms(
        args.repository, args.siteroot
    )
    print(json.dumps(projects_platforms, indent=4, sort_keys=True))


def _cmp_project_platforms_dict(data1, data2):
    """Comparing the content of the platforms lists for test purposes"""

    p1 = set(data1.keys())
    p2 = set(data2.keys())

    if p1 == p2:
        logging.info("Project list identical")
    else:
        if len(p1 - p2) > 0:
            logging.error("Different project list - Only in 1: %s", p1 - p2)
        if len(p2 - p1) > 0:
            logging.error("Different project list - Only in 2: %s", p2 - p1)
        # We don't compare the reverse as the new generated file is a subset with applications
        # in any case
        logging.error("Checking whether the content on file1 is identical to file2")

    # Now iterating all the projects in file1
    for p in p1:
        v1 = data1[p].keys()
        v2 = data2[p].keys()
        if v1 == v2:
            logging.info("Versions for %s are identical", p)
        else:
            if len(v1 - v2) > 0:
                logging.error(
                    "Different version list for %s - Only in 1: %s", p, v1 - v2
                )
            if len(v2 - v1) > 0:
                logging.error(
                    "Different version list for %s - Only in 2: %s", p, v2 - v1
                )

        for v in v1:
            p1 = set(data1[p][v])
            p2 = set(data2[p].get(v, []))
            # print(">>", p1)
            # print(">>", p2)
            if p1 == p2:
                logging.info("Platforms for %s %s are identical", p, v)
            else:
                if len(p1 - p2) > 0:
                    logging.error(
                        "Different platform list for %s %s - Only in 1: %s",
                        p,
                        v,
                        p1 - p2,
                    )
                if len(p2 - p1) > 0:
                    logging.error(
                        "Different platform list for %s %s - Only in 2: %s",
                        p,
                        v,
                        p2 - p1,
                    )


def compare_projects_platforms_json():
    parser = argparse.ArgumentParser()
    parser.add_argument("file1")
    parser.add_argument("file2")
    args = parser.parse_args()
    with open(args.file1, encoding="utf-8") as f1:
        with open(args.file2, encoding="utf-8") as f2:
            data1 = json.load(f1)
            data2 = json.load(f2)
            _cmp_project_platforms_dict(data1, data2)


def projects_info_json():
    """Command line LHCb project information to a JSON file"""
    parser = argparse.ArgumentParser()
    parser.add_argument("siteroot")
    parser.add_argument("repository")
    args = parser.parse_args()
    info = lhcbstacks.list_projects_info(args.repository, args.siteroot)
    print(json.dumps(info, indent=4, sort_keys=True))
