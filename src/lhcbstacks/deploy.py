#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function

import argparse
import os

import requests


def update_metadata(pipeline_id, token, method):
    response = requests.put(
        "https://lhcb-core-tasks.web.cern.ch/hooks/softmetadata"
        f"/{method}/{pipeline_id}/",
        headers={"Authorization": "Bearer " + token},
        timeout=5,
    )
    response.raise_for_status()
    task_id = response.json()
    print(f"Deploying {method} in {task_id}")

    while True:
        response = requests.get(
            f"https://lhcb-core-tasks.web.cern.ch/tasks/status/{task_id}",
            timeout=5,
        )
        if not response.ok:
            print(f"Got response code {response.status_code}")
            continue
        status = response.json()
        if status == "SUCCESS":
            print("Deployed sucessfully")
            break
        if status == "FAILURE":
            raise RuntimeError(
                f"See https://lhcb-core-tasks.web.cern.ch/tasks/result/{task_id}"
            )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pipeline_id", help="ID of the gitlab-ci pipeline")
    parser.add_argument(
        "--token",
        default=os.environ.get("REQUEST_DEPLOY_URL_TOKEN", None),
        help="Token for the softmetadata hook in lbtaskweb",
    )
    args = parser.parse_args()
    if args.token is None:
        raise RuntimeError(
            "Please specify the access token with REQUEST_DEPLOY_URL_TOKEN variable or with --token"
        )
    update_metadata(args.pipeline_id, args.token, "update_project_platforms")
    update_metadata(args.pipeline_id, args.token, "update_project_info")
    update_metadata(args.pipeline_id, args.token, "update_lcg_info")


if __name__ == "__main__":
    main()
