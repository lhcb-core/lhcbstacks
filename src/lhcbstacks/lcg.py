###############################################################################
# (c) Copyright 2021-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import functools
import json
import logging
import re
from pathlib import Path
from urllib.request import urlopen


def find_cmt_ROOT_version(siteroot, lcgversion):
    """Try in old structure to find ROOT version e.g. in requirements file like:
    /cvmfs/lhcb.cern.ch/lib/lcg/app/releases/LCGCMT/LCGCMT_64/LCG_Configuration/cmt/requirements
    """

    lcgreq = (
        Path(siteroot)
        / "lcg"
        / "app"
        / "releases"
        / "LCGCMT"
        / f"LCGCMT_{lcgversion}"
        / "LCG_Configuration"
        / "cmt"
        / "requirements"
    )
    if not lcgreq.exists():
        logging.debug(
            "LCG CMT requirements file %s does not exist, trying externals directory",
            lcgreq,
        )
        lcgreq = (
            Path(siteroot)
            / "lcg"
            / "external"
            / "LCGCMT"
            / f"LCGCMT_{lcgversion}"
            / "LCG_Configuration"
            / "cmt"
            / "requirements"
        )
        if not lcgreq.exists():
            logging.debug(
                "LCG CMT requirements file %s does not exist, not a cmt directory",
                lcgreq,
            )
            return None

    with open(lcgreq, encoding="UTF-8") as f:
        for line in f:
            if "ROOT_config_version" in line:
                v = line.split()[2].replace('"', "")
                logging.debug("ROOT version %s found in %s", v, lcgreq)
                return v
    return None


MAIN_URL = "https://lcgpackages.web.cern.ch/lcg/meta/"


# We cache this method to get the page only once
@functools.lru_cache
def _get_list_page():
    logging.debug("Get URL %s", MAIN_URL)
    return [line.decode("utf-8") for line in urlopen(MAIN_URL)]


@functools.lru_cache
def _get_lcg_page(lcgpage):
    logging.debug("Get URL %s", MAIN_URL + lcgpage)
    return [line.decode("utf-8") for line in urlopen(MAIN_URL + lcgpage)]


def _get_online_lcgpage_name(lcgversion):
    result = _get_list_page()

    for line in result:
        if f"LCG_{lcgversion}_LHCB" in line:
            m = re.search(r'<a href="(.*)">', line)
            if m:
                return m.group(1)
    raise RuntimeError(f"Could not find LCG_{lcgversion}_LHCB in MAIN_URL")


def find_online_ROOT_version(lcgversion):
    lcgpage = _get_online_lcgpage_name(lcgversion)
    logging.debug("Using metadata for %s", lcgpage)
    result = _get_lcg_page(lcgpage)
    for line in result:
        if "ROOT" in line:
            if re.search(r"NAME:\s*ROOT", line):
                # We have the right line, now we need to find the version
                m = re.search(
                    r"VERSION:\s*([^,]*?),", line
                )  # matchall chers except comma
                if m:
                    return m.group(1)
                raise RuntimeError(f"Could not find VERSION in {line}")
    return None


def find_ROOT_version(siteroot, lcgversion):
    """Main method to identify the ROOT version from LCG version

    It tries in different places according to the build tool (cmt or CMake),
    or depending on the file structure on disk.

    - for very old releases, it searched the CMT requirements file in:
      "/cvmfs/lhcb.cern.ch/lib/lcg/external/LCGCMT/LCGCMT_XXX/LCG_Configuration/cmt/requirements"

    - for posterior cmt releases it looks for
      "/cvmfs/lhcb.cern.ch/lib/lcg/app/releases/LCGCMT/LCGCMT_XXX/LCG_Configuration/cmt/requirements"

    - for CMake releases with a /cvmfs/lhcb.cern.ch/lib/lcg/LCG_XXX/ROOT directory,
      it looks there for the ROOT version

    - for more recent releases where we only have the packages with hashes in the name, not the symlinks,
      it looks online in the EP-SFT metadata site."""

    releasesdir = Path(siteroot) / "lcg" / "releases"

    if not releasesdir.exists():
        return ValueError(f"LCG directory {releasesdir} does not exist")

    lcgdir = releasesdir / f"LCG_{lcgversion}"
    if not lcgdir.exists():
        logging.debug("LCG directory %s does not exist, trying CMT metadata", lcgdir)
        version = find_cmt_ROOT_version(siteroot, lcgversion)
        if version:
            return version

        logging.debug("Could not find LCG metadata, trying online metadata")
        version = find_online_ROOT_version(lcgversion)
        if version:
            return version
        return None

    logging.debug("LCG directory %s exists", lcgdir)
    rootdir = lcgdir / "ROOT"
    for d in rootdir.iterdir():
        if d.is_dir() and not d.name.startswith("."):
            logging.debug("ROOT version %s found", d.name)
            return d.name

    return None


def get_cached_lcg_info(siteroot):
    lcg_info_file = Path(siteroot) / "var" / "lib" / "softmetadata" / "lcg-info.json"
    if not lcg_info_file.exists():
        raise FileNotFoundError(f"LCG info file {lcg_info_file} does not exist")
    return json.loads(lcg_info_file.read_text(encoding="UTF-8"))
