###############################################################################
# (c) Copyright 2021-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import re
import typing
from collections import defaultdict
from pathlib import Path

from packaging.version import Version
from packaging.version import parse as parse_version
from strictyaml import Map, MapPattern, Optional, Seq, Str, load

from .lcg import find_ROOT_version

#
# Set of tools to deal with the stack information in YAML,
# and comparing this ionformations with the releases

STACK_SCHEMA = Map(
    {
        Optional("name"): Str(),
        "type": Str(),
        "projects": MapPattern(Str(), Str()),
        "toolchain": Map({"type": Str(), "version": Str()}),
        "platforms": Seq(Str()),
        "tags": Seq(Str()),
    }
)


def stack_filename(repository, name):
    """Returns the file name for the stack"""
    return Path(repository) / f"{name}.yml"


def _load_stack_file(filename):
    """Load stack information by name from the specified repository"""
    with open(filename, encoding="utf-8") as file:
        info = load(file.read(), STACK_SCHEMA)
        return info.data


def _list_stacks_in_dir(dirname):
    """Returns the list of stack objects as (name, path) in the directory specified"""
    for path, _, files in os.walk(dirname):
        for f in files:
            if f.endswith(".yml"):
                fullname = os.path.relpath(Path(path) / f, start=dirname)[
                    : -len(".yml")
                ]
                yield fullname


def incomplete_stacks(repository, siteroot, stack_list=None):
    """List all the projects in all the defined stacks in the yaml repository"""
    inc_stacks = []

    if stack_list is None:
        stack_list = list(_list_stacks_in_dir(repository))

    for name in stack_list:
        s = Stack.load(repository, name, siteroot)
        if s.has_missing_projects():
            inc_stacks.append(s)

    return inc_stacks


def list_stacks_to_release(repository, siteroot, stack_list=None):
    """Generate the JSON file with the stacks that should be released"""
    # 1. Get the list of stacks with missing projects
    inc_stacks = incomplete_stacks(repository, siteroot, stack_list)

    # 2. Iterate to generate the stack in the exported format
    tobuild = []
    for stack in inc_stacks:
        for stack_number, (platform, project_list) in enumerate(
            sorted(stack.builds_to_do())
        ):
            # First checking if another build has the same projects exactly
            # Then add platform to it instead of creating a new build
            # The sort is to have a stable order in order to keep the stack
            # number consistent.
            found_matching = False
            for d, s in tobuild:
                if set(project_list) == s:
                    d["platforms"].append(platform)
                    found_matching = True
                    break

            if found_matching:
                continue

            # No match, so here we prepare the document listing the work
            # project list is the list to build
            # In this file for the nightlies we list the whole stack config
            # and the one already done (so the complement of the info we have)
            data = {}
            data["build_tool"] = stack.build_tool()
            data["platforms"] = [platform]
            data["projects"] = stack.projects()
            data["deployed"] = {
                p: v for p, v in stack.projects().items() if (p, v) not in project_list
            }
            if stack_number:
                data["name"] = f"{stack.name()}-{stack_number}"
            else:
                data["name"] = stack.name()
            tobuild.append((data, set(project_list)))

    return sorted([d for d, _ in tobuild], key=lambda d: d["name"])


def list_projects_platforms(repository, siteroot):
    """Generate the JSON file with the list of projects and the platforms
    available for all of them"""
    projects = {}

    def process_file(repository, name):
        s = Stack.load(repository, name, siteroot)
        for raw_p, v in s.projects().items():
            p = raw_p.upper()
            current_dict = projects.get(p, None)
            if current_dict is None:
                current_dict = {}
                projects[p] = current_dict
            current_dict[v] = s.platforms()

    for name in _list_stacks_in_dir(repository):
        process_file(repository, name)

    return projects


LCG = "LCG"


def _parse_lhcb_version(version: str) -> typing.Union[Version, bool]:
    """Parse a version string into a Version object"""
    return Version(re.sub(r"[^\d]+", ".", version).strip("."))


def _zstd_support(rootversion, gaudiversion):
    """Method that hardcodes the known compatibility of zstd
    according to the ROOT and Gaudi versions"""

    no_support = {
        "read_zstd": False,
        "write_zstd": False,
    }

    if rootversion is None or gaudiversion is None:
        return no_support

    try:
        rootv = parse_version(rootversion)
    except Exception:
        # Old versions of ROOT have weird naming,
        # this is not a problem as they do not support zstd anyway
        return no_support

    gaudiv = _parse_lhcb_version(gaudiversion)

    read_zstd = rootv > Version("6.20")
    write_zstd = read_zstd and gaudiv > Version("36.6")

    return {
        "read_zstd": read_zstd,
        "write_zstd": write_zstd,
    }


def list_projects_info(repository, siteroot):
    """Generate the JSON file with project information"""
    projects = {}

    def process_file(repository, name):
        s = Stack.load(repository, name, siteroot)

        for raw_p, v in s.projects().items():
            p = raw_p.upper()
            if p == LCG:
                # we skip the LCG project as we are only interested in
                # LHCb projects here.
                continue

            # Lookup the current metadata for the project
            current_dict = projects.get(p, None)
            if current_dict is None:
                current_dict = {}
                projects[p] = current_dict

            # toolchain and platforms
            (lcgname, lcgversion) = s.toolchain()
            if lcgname != LCG:
                raise ValueError(f"Toolchain {lcgname} not supported")
            project_data = {lcgname: lcgversion}
            project_data["platforms"] = s.platforms()

            # Adding GAUDI information
            if p != "GAUDI":
                # make sure the keys are uppercase

                gaudiversion = {k.upper(): v for k, v in s.projects().items()}.get(
                    "GAUDI", None
                )
                if gaudiversion is not None:
                    project_data["GAUDI"] = gaudiversion
            else:
                gaudiversion = v

            # Getting the ROOT version
            rootversion = find_ROOT_version(siteroot, lcgversion)
            project_data["ROOT"] = rootversion

            # Getting the ZSTD support
            project_data.update(_zstd_support(rootversion, gaudiversion))

            # Adding the project information to the dictionary
            current_dict[v] = project_data

    for name in _list_stacks_in_dir(repository):
        process_file(repository, name)

    return projects


class Stack:
    @classmethod
    def load(cls, stack_repo, stack_name, siteroot):
        """Factory method returning a stack object of the correct type"""
        stack_name = (
            stack_name[: -len(".yml")] if stack_name.endswith(".yml") else stack_name
        )
        stack_file = f"{stack_name}.yml"
        filename = Path(stack_repo) / stack_file
        info = _load_stack_file(filename)
        info["name"] = stack_name
        stack = None
        stack_type = info["type"].lower()
        if stack_type == "lhcbcmake":
            stack = LHCbClassicStack(siteroot, info, "cmake")
        elif stack_type == "cmt":
            stack = LHCbClassicStack(siteroot, info, "cmt")
        else:
            raise NotImplementedError(f"Stack type {info['type']} not implemented")
        return stack

    def __init__(self, siteroot, stack_info):
        self.siteroot_path = Path(siteroot)
        self.info = stack_info

    def lhcb_dir(self):
        """top directory for the installation of projects in the siteroot"""
        return self.siteroot_path / "lhcb"

    def platforms(self):
        return self.info["platforms"]

    def projects(self):
        tmp = self.info["projects"].copy()
        tctype, tcversion = self.toolchain()
        tmp[tctype] = tcversion
        return tmp

    def name(self):
        return self.info["name"]

    def missing_projects(self):
        """Check on disk whether the projects actually exist"""
        missing = defaultdict(list)
        for binary_tag, paths in self.binary_paths().items():
            for p, project, version in paths:
                if not os.path.exists(p):
                    missing[binary_tag].append((p, project, version))
        return missing

    def has_missing_projects(self):
        """return true if some projects are missing"""
        return len(self.missing_projects()) > 0

    def toolchain(self):
        """Accessor to the toolchain information"""
        toolchain = self.info["toolchain"]
        return (toolchain["type"], str(toolchain["version"]))

    def binary_paths(self):
        raise NotImplementedError()

    def build_tool(self):
        raise NotImplementedError()

    def builds_to_do(self):
        raise NotImplementedError()


class LHCbClassicStack(Stack):
    """Class representing a stack built with classic LHCb CMake configuration"""

    def __init__(self, siteroot, stack_info, build_tool):
        super().__init__(siteroot, stack_info)
        self._build_tool = build_tool

    def project_paths(self):
        """Build the list of installation paths for the projects in the stack"""
        return [
            (self.lhcb_dir() / f"{p.upper()}" / f"{p.upper()}_{v}", p, v)
            for p, v in self.info["projects"].items()
        ]

    def binary_paths(self):
        """Build the list of installation paths for the projects in the stack for all platforms"""
        return {
            b: [
                (p / "InstallArea" / b, project, version)
                for (p, project, version) in self.project_paths()
            ]
            for b in self.platforms()
        }

    def build_tool(self):
        return self._build_tool

    def builds_to_do(self):
        """Organize the builds in consistent stacks that the LHCb nightly builds can process"""
        missing = self.missing_projects()
        todo = []
        for platform, project_list in missing.items():
            tmplist = [(p, v) for (_, p, v) in project_list]
            todo.append((platform, tmplist))
        return todo


PROJECTS_SCHEMA = Map(
    {
        "defaultSourceURI": Str(),
        "projects": Seq(Map({"name": Str(), Optional("sourceuri"): Str()})),
    }
)


def list_projects(projects_data):
    """Returns the list of known projects"""
    info = None
    with open(projects_data, encoding="utf-8") as file:
        info = load(file.read(), PROJECTS_SCHEMA).data
    defaultSourceURI = info["defaultSourceURI"]
    projects = info["projects"]
    for p in projects:
        if "sourceuri" not in p:
            p["sourceuri"] = defaultSourceURI + "/" + p["name"]
    return projects
